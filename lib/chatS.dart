import 'package:firebase/chat.dart';
import 'package:flutter/material.dart';

class HomePageC extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text("Chat App"),
        ),
        body: new ChatScreen());
  }
}
