//import 'package:firebase/ChatScreen.dart';
import 'package:firebase/chat_1.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';

final googleSignIn = new GoogleSignIn();
final analytics = new FirebaseAnalytics();

class FirebaseAnalytics {
}
final auth = FirebaseAuth.instance;
var currentUserEmail;
var _scaffoldContext;

final TextEditingController _textController = new TextEditingController();
final List<ChatMessage> _messages = <ChatMessage>[];




class ChatScreen extends StatefulWidget {
  @override
  State createState() => new ChatScreenState();
}

class ChatScreenState extends State<ChatScreen> {

  final TextEditingController _textController = new TextEditingController();
  final List<ChatMessage> _messages = <ChatMessage>[];

  void _handleSubmitted(String text) {
    _textController.clear();
    ChatMessage message = new ChatMessage(
      text: text,
    );
    setState(() {
      _messages.insert(0, message);
    });
  }
  final TextEditingController _textEditingController =
  new TextEditingController();

  // final TextEditingController _textController = new TextEditingController();

  bool _isComposingMessage = false;
  final reference = FirebaseDatabase.instance.reference().child('messages');

  // loading msg

//  void _handleSubmitted(String text) {
//    _textController.clear();
//    ChatMessage message = new ChatMessage(
//      text: text,
//    );
//    setState(() {
//      _messages.insert(0, message);
//    });
//  }


  Widget _textComposerWidget() {
    return new IconTheme(
      data: new IconThemeData(color: Colors.blue),
      child: new Container(
        margin: const EdgeInsets.symmetric(horizontal: 8.0),
        child: new Row(
          children: <Widget>[

            new Flexible(
              child: new TextField(
                decoration:
                new InputDecoration.collapsed(hintText: "Send a message"),
                controller: _textController,
                onSubmitted: _handleSubmitted,
              ),
            ),
            new Flexible(
             // margin: const EdgeInsets.symmetric(horizontal: 4.0),
              child: new IconButton(
                icon: new Icon(Icons.send),
                onPressed: () => _handleSubmitted(_textController.text),
              ),
            ),

            /*new Flexible(
              child: new FirebaseAnimatedList(
                query: reference,
                padding: const EdgeInsets.all(8.0),
                reverse: true,
                sort: (a, b) => b.key.compareTo(a.key),
                //comparing timestamp of messages to check which one would appear first
                *//*itemBuilder: (_, DataSnapshot messageSnapshot,
                    Animation<double> animation) {
                  return new ChatMessageListItem(
                    messageSnapshot: messageSnapshot,
                    animation: animation,
                  );
                },*//*
              ),
            ),*/
            new Flexible(
              child: new IconButton(
                  icon: new Icon(
                    Icons.photo_camera,
                    color: Theme.of(context).accentColor,
                  ),
                  onPressed: (){},
                /* async {
                    await _ensureLoggedIn();
                    File imageFile = await ImagePicker.pickImage();
                    int timestamp = new DateTime.now().millisecondsSinceEpoch;
                    StorageReference storageReference = FirebaseStorage
                        .instance
                        .ref()
                        .child("img_" + timestamp.toString() + ".jpg");
                    StorageUploadTask uploadTask =
                    storageReference.put(imageFile);
                    Uri downloadUrl = (await uploadTask.future).downloadUrl;
                    _sendMessage(
                        messageText: null, imageUrl: downloadUrl.toString());
                  }*/
                  ),
            ),


            new Flexible(

              child: Padding(
                padding: const EdgeInsets.all(10.0),
                //width: 100.0,
                child: new TextField(
                    keyboardType: TextInputType.multiline,
                    maxLines: 3,
                    decoration:
                    new InputDecoration.collapsed(hintText: "Write a Message"),
                    onChanged: (String messageText) {
                      setState(() {
                        _isComposingMessage = messageText.length > 0;
                      });
                    },
                    //controller: _textController,
                    onSubmitted: _textMessageSubmitted,

                    style: new TextStyle(
                      fontSize: 16.0,
                        height: 1.0,

                        color: Colors.black
                    )
                ),
              ),
            ),
            new Container(

            ),
            new Container(
              margin: const EdgeInsets.symmetric(horizontal: 4.0),
              child: new IconButton(
                icon: new Icon(Icons.send),
                onPressed: _isComposingMessage
                    ? () => _textMessageSubmitted(_textEditingController.text)
                    : null,

                //onPressed: (){handleSubmit();}
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Column(
      children: <Widget>[
        new Flexible(
          child: new ListView.builder(
            padding: new EdgeInsets.all(8.0),
            reverse: true,
            itemBuilder: (_, int index) => _messages[index],
            itemCount: _messages.length,
          ),
        ),
        new Divider(
          height: 1.0,
        ),
        new Container(
          decoration: new BoxDecoration(
            color: Theme.of(context).cardColor,
          ),
          child: _textComposerWidget(),
        ),

        new Flexible(
          child: new ListView.builder(
            padding: new EdgeInsets.all(8.0),
            reverse: true,
            itemBuilder: (_, int index) => _messages[index],
            itemCount: _messages.length,
          ),
        ),
        new Divider(
          height: 1.0,
        ),
        new Container(
          decoration: new BoxDecoration(
            color: Theme
                .of(context)
                .cardColor,
          ),
          child: _textComposerWidget(),
        ),
      ],
    );
  }


Future<Null> _textMessageSubmitted(String text) async {
  _textEditingController.clear();

  setState(() {
    _isComposingMessage = false;
  });

  await _ensureLoggedIn();
  _sendMessage(messageText: text, imageUrl: null);
}

  void _sendMessage({String messageText, String imageUrl}) {
    reference.push().set({
      'text': messageText,
      //'email': googleSignIn.currentUser.email,
      //'imageUrl': imageUrl,
      'senderName': googleSignIn.currentUser.displayName,
     // 'senderPhotoUrl': googleSignIn.currentUser.photoUrl,
    });

    //analytics.logEvent(name: 'send_message');
  }

  Future<Null> _ensureLoggedIn() async {
    GoogleSignInAccount signedInUser = googleSignIn.currentUser;
    if (signedInUser == null)
      signedInUser = await googleSignIn.signInSilently();
    if (signedInUser == null) {
      await googleSignIn.signIn();
      // analytics.logLogin();
    }

    currentUserEmail = googleSignIn.currentUser.email;

    if (await auth.currentUser() == null) {
      GoogleSignInAuthentication credentials =
      await googleSignIn.currentUser.authentication;
      await auth.signInWithGoogle(
          idToken: credentials.idToken, accessToken: credentials.accessToken);
    }
  }

  Future _signOut() async {
    await auth.signOut();
    googleSignIn.signOut();
//  Scaffold
//      .of(_scaffoldContext)
//      .showSnackBar(new SnackBar(content: new Text('User logged out')));
  }

}