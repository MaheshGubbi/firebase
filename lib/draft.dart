
import 'package:firebase/auth_provider.dart';
import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_auth/firebase_auth.dart';



class EmailFieldValidator {
  static String validate(String value) {
    return value.isEmpty ? 'Email can\'t be empty' : null;
  }
}


class HomePage extends StatelessWidget {
  final formKey = GlobalKey<FormState>();

  String nname;
  String _userId;
  String _name= "person 1";

  bool validateAndSave() {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }


  HomePage({this.onSignedOut});
  final VoidCallback onSignedOut;

  void _signOut(BuildContext context) async {
    try {
      var auth = AuthProvider.of(context).auth;
      await auth.signOut();
      onSignedOut();
    } catch (e) {
      print(e);
    }
  }



  @override
  Widget build(BuildContext context) {

    FirebaseAuth.instance.currentUser().then((user) {
      _userId = user.uid;

    });


    return Scaffold(
        appBar: AppBar(
          title: Text('Welcome'),
          actions: <Widget>[
            FlatButton(
                child: Text('Logout',
                    style: TextStyle(fontSize: 17.0, color: Colors.white)),
                onPressed: () => _signOut(context))
          ],
        ),
        body: Container(
          child: Column(
            //crossAxisAlignment: CrossAxisAlignment.stretch,
            children: buildInputs(),
            /*children: <Widget>[

              TextFormField(
                key: Key('name'),
                decoration: InputDecoration(labelText: 'Name'),
               // validator: EmailFieldValidator.validate,
                onSaved: (value) => nname = value,

              ),

              RaisedButton(
                key: Key('save'),
                child: Text('save', style: TextStyle(fontSize: 20.0)),
                onPressed: nameS, //validateAndSubmit,
              ),

            ],*/

          ),
        ));
  }


  List<Widget> buildInputs() {
    return [
      TextFormField(
        key: Key('email'),
        decoration: InputDecoration(labelText: 'Name'),
        validator: EmailFieldValidator.validate,
        onSaved: (value) => _name = value,

      ),
      RaisedButton(
          key: Key('save'),
          child: Text('save', style: TextStyle(fontSize: 20.0)),
          onPressed: nameS//validateAndSubmit,
      ),
    ];
  }

  void nameS(){
    if (validateAndSave()) {
      final DatabaseReference database = FirebaseDatabase.instance.reference()
          .child("$_userId");
      {
        database.set({
          'name': 'jaaaaaaaa',
        });
      }
    }
  }
}

