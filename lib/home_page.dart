import 'package:firebase/auth_provider.dart';
import 'package:firebase/contact.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_auth/firebase_auth.dart';


class EmailFieldValidator {
  static String validate(String value) {
    return value.isEmpty ? 'Email can\'t be empty' : null;
  }
}


class HomePage extends StatelessWidget {

  HomePage({this.onSignedOut});

  final VoidCallback onSignedOut;

  void _signOut(BuildContext context) async {
    try {
      var auth = AuthProvider
          .of(context)
          .auth;
      await auth.signOut();
      onSignedOut();
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'chat Demo',
     // theme: ThemeData.dark(),
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: home(),
    );
    /*return MaterialApp(
      child: Scaffold(
          appBar: AppBar(
            title: Text('Chat App'),
            actions: <Widget>[

              IconButton(
                icon: Icon(Icons.add),
                onPressed: null,
              ),

//            FlatButton(
//                child: Text('Logout',
//                    style: TextStyle(fontSize: 17.0, color: Colors.white)),
//                onPressed: () => _signOut(context)),
              PopupMenuButton(
                itemBuilder: (BuildContext context) {
                  return [
                    PopupMenuItem(child: Text('Settings')),
                    PopupMenuItem(child: Text('Logout')
                    )

                  ];
                },
              ),
            ],
          ),

       ),
    );*/
  }
}

////
class home extends StatefulWidget {
  @override
  _homeState createState() => _homeState();
}

class _homeState extends State<home> {

  List<Item> Users = List();
  Item item;
  DatabaseReference itemRef;
  String N;
  String person_name;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final FirebaseDatabase database = FirebaseDatabase.instance; //Rather then just writing FirebaseDatabase(), get the instance.


  @override
  void initState() {
    super.initState();
    item = Item("", "");
    itemRef = database.reference().child('Users');
    itemRef.onChildAdded.listen(_onEntryAdded);
    itemRef.onChildChanged.listen(_onEntryChanged);
  }

  _onEntryAdded(Event event) {
    setState(() {
      Users.add(Item.fromSnapshot(event.snapshot));
    });
  }

  _onEntryChanged(Event event) {
    var old = Users.singleWhere((entry) {
      return entry.key == event.snapshot.key;
    });
    setState(() {
      Users[Users.indexOf(old)] = Item.fromSnapshot(event.snapshot);
    });
  }

  void handleSubmit() {
    String ss =item.name;


    final FormState form = formKey.currentState;

    if (form.validate()) {
      form.save();
      form.reset();
      itemRef.push().set(item.toJson());
    }
  }
/*
  void ok(){
    final FirebaseDatabase database = FirebaseDatabase.instance;
    itemRef.child(item.name);
  }*/


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Chat App'),
        actions: <Widget>[

          IconButton(
            icon: Icon(Icons.add),
            onPressed: null,
          ),

//            FlatButton(
//                child: Text('Logout',
//                    style: TextStyle(fontSize: 17.0, color: Colors.white)),
//                onPressed: () => _signOut(context)),
          PopupMenuButton(
            itemBuilder: (BuildContext context) {
              return [
                PopupMenuItem(child: Text('Settings')),
                PopupMenuItem(child: Text('Logout'),
                )
              ];
            },
          ),
        ],

      ),
      resizeToAvoidBottomPadding: false,
      body: Column(
        children: <Widget>[
          Flexible(
            flex: 0,
            child: Padding(
              padding: EdgeInsetsDirectional.fromSTEB(0.0, 50.0, 0.0, 0.0),
              child: Center(
                child: Form(
                  key: formKey,
                  child: Flex(
                    direction: Axis.vertical,
                    children: <Widget>[

                      ListTile(
                        //leading: Icon(Icons.info),
                        title:
                        TextFormField(
                          //controller: person,
                          decoration: InputDecoration(labelText: 'Name'),
                          //initialValue: 'Enter your name',
                          onSaved: (val) => item.name = val,
                          validator: (val) => val == "" ? val : null,
                        ),
                      ),
                      ListTile(
                        title: TextFormField(
                          decoration: InputDecoration(labelText: 'Number',),
                          initialValue: "",
                          onSaved: (val) => item.Number = val,
                          validator: (val) => val == "" ? val : null,
                        ),
                      ),
                      RaisedButton(
                        key: Key('signIn'),
                        child: Text('Save', style: TextStyle(fontSize: 20.0)),
                        onPressed: () {

                          // itemRef = database.reference().child('Users').child('item_name');
                          handleSubmit();
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => Contact()),
                          );
                        },
                      ),


                      RaisedButton(
                        child: Text('New Message', style: TextStyle(fontSize: 20.0)),
                        onPressed: () {

                          // itemRef = database.reference().child('Users').child('item_name');
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => Contact()),
                          );
                        },
                      ),

                      /*IconButton(
                        //decoration: InputDecoration(labelText: 'Email'),
                        icon: Icon(Icons.save),
                        onPressed: () {
                          handleSubmit();
                        },
                      ),*/
                    ],
                  ),
                ),
              ),
            ),
          ),
          /*Flexible(
            child: FirebaseAnimatedList(
              query: itemRef,
              itemBuilder: (BuildContext context, DataSnapshot snapshot,
                  Animation<double> animation, int index) {
                return new ListTile(
                  leading: Icon(Icons.person),
                  title: Text(items[index].title),
                  subtitle: Text(items[index].body),
                );
              },
            ),
          ),*/
        ],
      ),
    );
  }
}


class Item {
  String key;
  String name;
  String Number;

  Item(this.name, this.Number);

  Item.fromSnapshot(DataSnapshot snapshot)
      : key = snapshot.key,
        name = snapshot.value["name"],
        Number = snapshot.value["body"];

  toJson() {
    return {

      "name": name,
      "Number": Number,
    };
  }
}


/*
fdbRefer.addListenerForSingleValueEvent(new ValueEventListener() {
  @Override
  public void onDataChange(DataSnapshot dataSnapshot) {
  if(dataSnapshot.exist() {
//username exist
  }
  else {
//username doesn't exist
  }
  }
});
*/
