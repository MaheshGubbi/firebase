
import 'package:firebase/chatS.dart';
import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
//import 'package:firebase_core/firebase_core.dart'; not nessecary



class Contact extends StatefulWidget {
  @override
  HomeState createState() => HomeState();
}

class HomeState extends State<Contact> {
  List<Item> Users = List();
  Item item;
  DatabaseReference itemRef;

  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    item = Item("", "");
    final FirebaseDatabase database = FirebaseDatabase.instance; //Rather then just writing FirebaseDatabase(), get the instance.
    itemRef = database.reference().child('Users');
    itemRef.onChildAdded.listen(_onEntryAdded);
    itemRef.onChildRemoved.listen(_onEntryRemoved);
    itemRef.onChildChanged.listen(_onEntryChanged);
  }

  _onEntryAdded(Event event) {
    setState(() {
      Users.add(Item.fromSnapshot(event.snapshot));
    });
  }

  _onEntryChanged(Event event) {
    var old = Users.singleWhere((entry) {
      return entry.key == event.snapshot.key;
    });
    setState(() {
      Users[Users.indexOf(old)] = Item.fromSnapshot(event.snapshot);
    });
  }

  _onEntryRemoved(Event event) {
    setState(() {
      Users.remove(Item.fromSnapshot(event.snapshot));
    });
  }

  void handleSubmit() {
    final FormState form = formKey.currentState;

    if (form.validate()) {
      form.save();
      form.reset();
      itemRef.push().set(item.toJson());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Contacts List'),
        actions: <Widget>[

          IconButton(
            icon: Icon(Icons.add),
            onPressed: null,
          ),

//            FlatButton(
//                child: Text('Logout',
//                    style: TextStyle(fontSize: 17.0, color: Colors.white)),
//                onPressed: () => _signOut(context)),
          PopupMenuButton(
            itemBuilder: (BuildContext context) {
              return [
                PopupMenuItem(child: Text('Settings')),
                PopupMenuItem(child: Text('Logout')
                )

              ];
            },
          ),
        ],
      ),
      resizeToAvoidBottomPadding: false,
      body: Column(
        children: <Widget>[
          Flexible(
            flex: 0,
            child: Center(
              child: Form(
                key: formKey,
                child: Flex(
                  direction: Axis.vertical,
                  children: <Widget>[/*
                    ListTile(
                      leading: Icon(Icons.info),
                      title: TextFormField(
                        initialValue: "",
                        onSaved: (val) => item.title = val,
                        validator: (val) => val == "" ? val : null,
                      ),
                    ),
                    ListTile(
                      leading: Icon(Icons.info),
                      title: TextFormField(
                        initialValue: '',
                        onSaved: (val) => item.body = val,
                        validator: (val) => val == "" ? val : null,
                      ),
                    ),
                    IconButton(
                      icon: Icon(Icons.send),
                      onPressed: () {
                        handleSubmit();
                      },
                    ),*/
                  ],
                ),
              ),
            ),
          ),
          Flexible(
            child: FirebaseAnimatedList(
              query: itemRef,
              itemBuilder: (BuildContext context, DataSnapshot snapshot,
                  Animation<double> animation, int index) {
                return new ListTile(
                  leading: Icon(Icons.person),
                  title: Text(Users[index].name,style: TextStyle(fontSize: 18.0),),

                  onTap: (){
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => HomePageC(),
                      ),
                    );
                    /*var route = new MaterialPageRoute(
                        builder: (BuildContext context) =>
                    new NextPage(value: _textController.text),


                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => HomePage(),
                      ),
                    );*/
                  },
                  onLongPress: (){
                    itemRef.onChildRemoved.listen(_onEntryRemoved);
                  },
                 // subtitle: Text(Users[index].body,),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}

class Item {
  String key;
  String name;
  String Number;

  Item(this.name, this.Number);

  Item.fromSnapshot(DataSnapshot snapshot)
      : key = snapshot.key,
        name = snapshot.value["name"],
        Number = snapshot.value["Number"];

  toJson() {
    return {
      "name": name,
      "Number": Number,
    };
  }
}


class HomePage extends StatelessWidget {
  final Item item;

  // In the constructor, require a Todo
  HomePage({Key key, @required this.item}) : super(key: key);

@override
Widget build(BuildContext context) {
  return Scaffold(
    appBar: AppBar(
      title: Text("${item.name}"),
    ),
    body: Padding(
      padding: EdgeInsets.all(16.0),
      child: Text('${item.Number}'),
    ),
  );
}
}
